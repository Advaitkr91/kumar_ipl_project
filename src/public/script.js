 function matchesPerYear() {
    let seriesData = []
    fetch("./output/matchesPerYear.json")
        .then(response => response.json())
        .then((data) => {

            const chartData = [];
            for (const key in data) {
                chartData.push([key, data[key]]);
            }

            Highcharts.chart('matches-played-per-year', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Matches Per Year'
                },
                subtitle: {
                    text: 'Source: <a href="http://en.wikipedia.org/wiki/List_of_cities_proper_by_population">Wikipedia</a>'
                },
                xAxis: {
                    type: 'category',
                    labels: {
                        rotation: -45,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Population (millions)'
                    }
                },
                legend: {
                    enabled: false
                },
                tooltip: {
                    pointFormat: 'Population in 2017: <b>{point.y:.1f} millions</b>'
                },
                series: [{
                    name: 'Population',
                    data: chartData,
                    dataLabels: {
                        enabled: true,
                        rotation: -90,
                        color: '#FFFFFF',
                        align: 'right',
                        format: '{point.y:.1f}', // one decimal
                        y: 10, // 10 pixels down from the top
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                }]
            });


        });
}
matchesPerYear()

function matchesWonPerYear() {

    fetch("./output/matchesWonPerYear.json")
    .then(response => response.json())
    .then((data) => {
        let finalResult = []
        const season = Object.keys(data).map((season)=>season);
        //console.log(season)
        const team = []
        for(let i=0;i<season.length;i++)
       { 
       team.push(Object.keys(data[season[i]]));
       }
       //console.log(team);
       teams = [...new Set([].concat.apply([],team))]
       console.log(teams);
       for(let i in teams){
        let tempArr = []
         for(let j in season){
           
           if(data[season[j]].hasOwnProperty(teams[i]))
             tempArr.push(data[season[j]][teams[i]])
             else tempArr.push(0)
          }
        finalResult.push({name:teams[i],data:tempArr});
     }

     Highcharts.chart("match-win", {
        chart: {
          renderTo: 'match-win',
          type: "column"
        },
        title: {
          text: "Matches Won"
        },
        subtitle: {
          text:
            'Source: <a href="https://www.kaggle.com/nowke9/ipldata/data">IPL Dataset</a>'
        },
        xAxis: {
          categories:[...season],
          crosshair: true
        
        },
        yAxis: {
          min: 0,
          title: {
            text: "Matches won vs stadium"
          }
        },
        tooltip: {
          headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
          pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td><td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
          footerFormat: "</table>",
          shared: true,
          useHTML: true
      },
      plotOptions: {
          column: {
              pointPadding: .2,
              borderWidth: 0
          }
      },
      series:finalResult
      });
    });
    }

matchesWonPerYear()
function extraRun(){

   fetch("./output/extraRunsPerTeam.json")
   .then(response=>response.json())
   .then((data)=>{
    let extraRunPerteam = []

    for(let team in data){
       //console.log(team)
       //console.log(data[team]);
       extraRunPerteam.push([team,data[team]])
       //console.log(extraRunPerteam)
    }

    Highcharts.chart('extra-run-per-team', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'extra run Per team in 2016'
        },
        subtitle: {
            text: 'Source: <a href="http://en.wikipedia.org/wiki/List_of_cities_proper_by_population">Wikipedia</a>'
        },
        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'extra run'
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: 'Population in 2017: <b>{point.y:.1f} millions</b>'
        },
        series: [{
            name: 'Population',
            data: extraRunPerteam,
            dataLabels: {
                enabled: true,
                rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                format: '{point.y:.1f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
   



   })

}
extraRun();

function topEconomicBowler(){

   fetch("./output/topEconomicBowlers.json")
   .then(response=>response.json())
   .then((data)=>{
     const recordEconomy = []
    for(let bowler in data){
         //console.log(bowler,data[bowler]);
          recordEconomy.push([bowler,parseFloat(data[bowler])])
          //console.log(recordEconomy)
    }
    
    Highcharts.chart('top-economic-bowler', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'top Economy 2016'
        },
        subtitle: {
            text: 'Source: <a href="http://en.wikipedia.org/wiki/List_of_cities_proper_by_population">Wikipedia</a>'
        },
        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Economy'
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: 'Population in 2017: <b>{point.y:.1f} millions</b>'
        },
        series: [{
            name: 'Bowler',
            data: recordEconomy,
            dataLabels: {
                enabled: true,
                rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                format: '{point.y:.1f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });

})
}
topEconomicBowler()
function tossWinteam(){
    fetch("./output/tossWinteam.json")
    .then(response => response.json())
    .then((data) =>{

        const tossTeamWin = [] 

        for(let team in data){

           tossTeamWin.push([team,data[team]])
           console.log(tossTeamWin)
      }


      Highcharts.chart('toss-team-win', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: 0,
            plotShadow: false
        },
        title: {
            text: 'tosswin-teamWin',
            align: 'center',
            verticalAlign: 'middle',
            y: 60
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        accessibility: {
            point: {
                valueSuffix: '%'
            }
        },
        plotOptions: {
            pie: {
                dataLabels: {
                    enabled: true,
                    distance: -50,
                    style: {
                        fontWeight: 'bold',
                        color: 'white'
                    }
                },
                startAngle: -90,
                endAngle: 90,
                center: ['50%', '75%'],
                size: '110%'
            }
        },
        series: [{
            type: 'pie',
            name: 'Browser share',
            innerSize: '50%',
            data: tossTeamWin,
                  
                /*{
                    name: 'Other',
                    y: 7.61,
                    dataLabels: {
                        enabled: false
                    }
                }*/
            
        }]
    });
   



    })

}
tossWinteam()
function highestPlayerOfTheSeason(){
    fetch("./output/highestPlayerOfTheSeason.json")
    .then(response => response.json())
    .then((data) =>{
     const highestPlayerAward = []
     
     for(let year in data){
          
           highestPlayerAward.push([parseFloat(year),data[year]])
           //console.log(parseFloat(year),data[year])
     }

     Highcharts.chart('highest-player-award', {
        chart: {
            height: 600,
            inverted: true
        },
    
        title: {
            text: 'Highcharts Org Chart'
        },
    
        accessibility: {
            point: {
                descriptionFormatter: function (point) {
                    var nodeName = point.toNode.name,
                        nodeId = point.toNode.id,
                        nodeDesc = nodeName === nodeId ? nodeName : nodeName + ', ' + nodeId,
                        parentDesc = point.fromNode.id;
                    return point.index + '. ' + nodeDesc + ', reports to ' + parentDesc + '.';
                }
            }
        },
    
        series: [{
            type: 'organization',
            name: 'Highsoft',
            keys: ['from', 'to'],
            data: highestPlayerAward,
            levels: [{
                level: 0,
                color: 'silver',
                dataLabels: {
                    color: 'black'
                },
                height: 25
            }, {
                level: 1,
                color: 'silver',
                dataLabels: {
                    color: 'black'
                },
                height: 25
            }, {
                level: 2,
                color: '#980104'
            }, {
                level: 4,
                color: '#359154'
            }],
            nodes: [{
                id: 'Shareholders'
            }, {
                id: 'Board'
            }, {
                id: 'CEO',
                title: 'CEO',
                name: 'Grethe Hjetland',
                image: 'https://wp-assets.highcharts.com/www-highcharts-com/blog/wp-content/uploads/2020/03/17131126/Highsoft_03862_.jpg'
            }, {
                id: 'HR',
                title: 'HR/CFO',
                name: 'Anne Jorunn Fjærestad',
                color: '#007ad0',
                image: 'https://wp-assets.highcharts.com/www-highcharts-com/blog/wp-content/uploads/2020/03/17131210/Highsoft_04045_.jpg'
            }, {
                id: 'CTO',
                title: 'CTO',
                name: 'Christer Vasseng',
                image: 'https://wp-assets.highcharts.com/www-highcharts-com/blog/wp-content/uploads/2020/03/17131120/Highsoft_04074_.jpg'
            }, {
                id: 'CPO',
                title: 'CPO',
                name: 'Torstein Hønsi',
                image: 'https://wp-assets.highcharts.com/www-highcharts-com/blog/wp-content/uploads/2020/03/17131213/Highsoft_03998_.jpg'
            }, {
                id: 'CSO',
                title: 'CSO',
                name: 'Anita Nesse',
                image: 'https://wp-assets.highcharts.com/www-highcharts-com/blog/wp-content/uploads/2020/03/17131156/Highsoft_03834_.jpg'
            }, {
                id: 'Product',
                name: 'Product developers'
            }, {
                id: 'Web',
                name: 'Web devs, sys admin'
            }, {
                id: 'Sales',
                name: 'Sales team'
            }, {
                id: 'Market',
                name: 'Marketing team',
                column: 5
            }],
            colorByPoint: false,
            color: '#007ad0',
            dataLabels: {
                color: 'white'
            },
            borderColor: 'white',
            nodeWidth: 65
        }],
        tooltip: {
            outside: true
        },
        exporting: {
            allowHTML: true,
            sourceWidth: 800,
            sourceHeight: 600
        }
    
    });
    

  })

}
highestPlayerOfTheSeason()

function strikeRate(){

    fetch("./output/strikeRates.json")
    .then(response=>response.json())
    .then((data)=>{

         const strikeRate = []
         for(let season in data){

               strikeRate.push([parseFloat(season),parseFloat(data[season])])
               console.log(strikeRate)
         }

         

            Highcharts.chart('strikeRate', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'strike Rate'
                },
                subtitle: {
                    text: 'Source: <a href="http://en.wikipedia.org/wiki/List_of_cities_proper_by_population">Wikipedia</a>'
                },
                xAxis: {
                    type: 'category',
                    labels: {
                        rotation: -45,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'rates'
                    }
                },
                legend: {
                    enabled: false
                },
                tooltip: {
                    pointFormat: 'Population in 2017: <b>{point.y:.1f} millions</b>'
                },
                series: [{
                    name: 'Population',
                    data: strikeRate,
                    dataLabels: {
                        enabled: true,
                        rotation: -90,
                        color: '#FFFFFF',
                        align: 'right',
                        format: '{point.y:.1f}', // one decimal
                        y: 10, // 10 pixels down from the top
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                }]
            });
         




    })


}
strikeRate()
function superOver(){

    fetch("./output/superOver.json")
    .then(response => response.json())
    .then((data)=>{
          const final_superOver = []
          for(let bowler in data){

               final_superOver.push([bowler,parseFloat(data[bowler])])
               console.log(final_superOver)
          }
          
          Highcharts.chart('superOver', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'strike Rate'
            },
            subtitle: {
                text: 'Source: <a href="http://en.wikipedia.org/wiki/List_of_cities_proper_by_population">Wikipedia</a>'
            },
            xAxis: {
                type: 'category',
                labels: {
                    rotation: -45,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'rates'
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                pointFormat: 'Population in 2017: <b>{point.y:.1f} millions</b>'
            },
            series: [{
                name: 'Population',
                data: final_superOver,
                dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: '#FFFFFF',
                    align: 'right',
                    format: '{point.y:.1f}', // one decimal
                    y: 10, // 10 pixels down from the top
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            }]
        });
     
     
   })


}
superOver()