exports.matchesPlayedPerYear =(matches)=>{
  const result = {}
  if(!matches || matches.length === 0) return []

      let matchesPlayedPerYear = matches.reduce((matchesPerYear,match)=>{

                if(matchesPerYear[match.season]>0){

                      matchesPerYear[match.season] += 1
                }
                else{

                    matchesPerYear[match.season] = 1
                }

                return matchesPerYear

      },{})
      console.log(matchesPlayedPerYear)
      return matchesPlayedPerYear
  }

  exports.matchesWonPerYear = (matches) =>{
     if(!matches || matches.length === 0) return []
      
     let WonPerYear = matches.reduce((matchesWon,match)=>{
       if(!matchesWon.hasOwnProperty(match.season)){

            matchesWon[match.season] = {}
       }
       if(!matchesWon[match.season].hasOwnProperty(match.team1)){

            matchesWon[match.season][match.team1] = 0
       }
       if(!matchesWon[match.season].hasOwnProperty(match.team2)){

          matchesWon[match.season][match.team2] = 0
       }
       if(matchesWon[match.season].hasOwnProperty(match.winner)){

               matchesWon[match.season][match.winner] += 1;
       }
          
          return matchesWon
     },{})
     console.log(WonPerYear)
     return WonPerYear            
  }

  exports.extraRuns=(deliveries,matches)=>{
       let result = {}
       let matchids = matches.filter(match => match.season === "2016")
       let match_ids = matchids.map(obj => obj.id)
       let delivery_id = deliveries.map(ar => ar.match_id)
       let deliveryid = [...new Set(delivery_id)]
       let temp = []
       let arr = []
       for(let id of match_ids)
       {
              for(let ids of deliveryid)
              {

                    if(id == ids){

                       temp.push(ids)
                  }
           }

       }
       for(let obj of deliveries){

            for(let r of temp){

                 if(obj.match_id === r){

                      arr.push(obj)
                 }
            }
       }
      for(let r of arr){
            if(result[r.bowling_team]){
                 result[r.bowling_team] += parseInt(r.extra_runs)
            }
            else{
                 result[r.bowling_team] = parseInt(r.extra_runs)
            }
      }
      console.log(result)
      return result
  }
 exports.topEconomicBowlers=(deliveries,matches)=>{
     
       const result = {}
       let final = []
       let season_ball = []
       let run_per_bowler = {}
       let match = matches.filter(obj => obj.season === "2015")
        
       for(let obj of match){
          for(let del of deliveries){

                if(obj.id === del.match_id){

                      season_ball.push(del)
                }
           }


       }

       for(let obj of season_ball){

            let bowler = obj.bowler
            if(result[bowler]){
                if(obj.wide_runs == 0 && obj.noball_runs == 0){

                       result[bowler] += 1
                }
                
            }
            else{

                  result[bowler] = 1
            }

       }

     for(let obj of season_ball){

           let bowler = obj.bowler
           let total_runs = obj.total_runs
           if(run_per_bowler[bowler]){

                 run_per_bowler[bowler] += parseInt(total_runs)
           }
           else{

               run_per_bowler[bowler] = parseInt(total_runs)
           }
     }
    for(let res in result){

          for(let run in run_per_bowler){

                 if(res == run){

                      let semi = []
                      let economy = 0;
                      economy = (run_per_bowler[run]/result[res])*6
                      let eco = economy.toFixed(2)
                      semi.push(run,eco)
                      final.push(semi)
                 }
          }
    }

    final.sort((a,b)=>a[1] - b[1])
    let score = final.slice(0,10)
    let final_economy = {}

    for(let i=0;i<score.length;i++)
     {

         if(!final_economy[score[i][0]])
         {
               
            final_economy[score[i][0]] = score[i][1]
         }
    }
    
    return final_economy
}
exports.tossWinteam = (matches)=>{
       let winTeam = {}

          for(let match of matches){
             if(!winTeam.hasOwnProperty(match.team1)){

                  winTeam[match.team1] = 0;
             }
             if(!winTeam.hasOwnProperty(match.team2)){

                 winTeam[match.team2] = 0
             }
           if(match.winner === match.toss_winner){

                 winTeam[match.winner] += 1
           }

     }
     
    console.log(winTeam)
    return winTeam
}
exports.highestPlayerOfTheSeason = (matches) =>{
     let result = {}
     for(let r of matches){

            season = r.season
            player_of_match = r.player_of_match
            if(result[season]){
                   
                 if(result[season][player_of_match]){
                    result[season][player_of_match] += 1   
                 }
                 else{
                    result[season][player_of_match] = 1
                 }
            }
            else{

                 result[season] = {}
                 result[season][player_of_match] = 1
            }
     }

    console.log(result)
    let highestPlayerAward = {}
     for(let r in result){
          
           let testarray = Object.entries(result[r])
           testarray.sort((a,b)=>b[1] - a[1])
           highestPlayerAward[r] = testarray[0][0]
    }

    console.log(highestPlayerAward)
    return highestPlayerAward
}
exports.strikeRates = (deliveries,matches) =>{
let records = {}
 for(let match of matches){
  for(let delivery of deliveries){
     if(match.id === delivery.match_id){
      if(records[match.season]){
        if(records[match.season][delivery.batsman]){
                 records[match.season][delivery.batsman].runs += parseInt(delivery.batsman_runs)
                     if(delivery.wide_runs === "0" && delivery.noball_runs === "0")
                     {
                       records[match.season][delivery.batsman].balls += 1;
                     }
                                        
                 }
                             
                 else{
                      records[match.season][delivery.batsman] = {runs:0,balls:0}
                      records[match.season][delivery.batsman].runs = parseInt(delivery.batsman_runs)
                      if(delivery.wide_runs === "0" && delivery.noball_runs === "0"){
                         records[match.season][delivery.batsman].balls = 1;
                       }
                                 
                    }
               }

               else{
                    records[match.season] = {}
               }
                   }
           }
     }  

     for(let season in records){
           for(let batsman in records[season]){

                 records[season][batsman].rate = ((records[season][batsman].runs/
                 records[season][batsman].balls)*100).toFixed(2);
           }
     }
     let finalrecords = []
     let final_strikeRate = {}
     for(let season in records){
         for(let name in records[season]){
                
                 finalrecords.push([name,records[season][name].rate,season])
               
         }
     }

     let datafinal = finalrecords.filter(ar=>ar[0] == 'V Kohli')
     for(let data of datafinal){

         final_strikeRate[data[2]] = data[1]

     }
     console.log(final_strikeRate)
     return final_strikeRate
}
exports.superOver = (deliveries)=>{

   let result = {}

   for(let r of deliveries){
     if(r.is_super_over > 0)
     {
        if(result[r.bowler]){

          result[r.bowler].runs += parseInt(r.noball_runs) + parseInt(r.wide_runs) + parseInt(r.batsman_runs)
             if(r.wide_runs === "0" && r.noball_runs === "0"){

                  result[r.bowler].balls += 1
             }
        }
        else{
             result[r.bowler] = {runs:0,balls:0}
             result[r.bowler].runs = parseInt(r.noball_runs) + parseInt(r.wide_runs) + parseInt(r.batsman_runs)
             if(r.wide_runs === "0" && r.noball_runs === "0"){

               result[r.bowler].balls = 1
          }

     }
 }

}
   console.log(result)
  let economy = {}
  for(let r in result){

     
     let over = (Math.floor(result[r].balls/6) + (result[r].balls%6) * 0.1).toFixed(2);
     let eco = (result[r].runs / parseFloat(over)).toFixed(2);
     result[r].economy = eco
     
     economy[r] = eco;
     
  }
  console.log(economy)
  let testeconomy = Object.entries(economy)
  testeconomy.sort((a,b)=>a[1]-b[1])
  console.log(testeconomy)
  let bestEconomy = []
   bestEconomy.push(testeconomy[0])
  console.log(bestEconomy) 
  let final_economy = {}
 for(let bowler of bestEconomy){

      final_economy[bowler[0]] = bowler[1]

 }
 return final_economy
}
exports.dismissedPlayer = (deliveries)=>{
let result = {}

for(let r of deliveries)
{   

if(r.player_dismissed.length > 0 && r.dismissal_kind.length > 0  && r.dismissal_kind.length !== "run out")
 {
    
     if(result[r.batsman]){

       if(result[r.batsman][r.bowler]){

            result[r.batsman][r.bowler] += 1
       }
       else{

            result[r.batsman][r.bowler] = {}
            result[r.batsman][r.bowler] = 1
       }
    }
    else{

         result[r.batsman] = {}
         result[r.batsman][r.bowler] = {}
          result[r.batsman][r.bowler].count = 1

    }
 }  
}
let finalRecord = {}
 let Max = 0,
 batting,
 bowling;
for(let batsman in result) {
let max = 0,
batsman_name,
bowler_name;
 for(let bowler in result[batsman]){

      if(max < result[batsman][bowler]){

          max = result[batsman][bowler];
          batsman_name = batsman
          bowler_name = bowler
      }
 
     }
     if(Max < max){

          Max = max;
          batting = batsman_name
          bowling = bowler_name
     }
    

   }
   console.log({finalcount : Max, bats_man:batting,bowlerfinal:bowling })
   return {finalcount : Max, bats_man:batting,bowlerfinal:bowling }

   

}






     
