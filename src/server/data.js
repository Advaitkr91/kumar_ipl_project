const path = require("path");
const fs = require("fs");
const csv = require("csv-parser")

exports.getMatchesData = (cb) => {
let matches = [];
let matchescsvPath = path.join(__dirname,"..","data","matches.csv");

fs.createReadStream(matchescsvPath)
   .pipe(csv())
   .on("data",(data)=> matches.push(data))
   .on("end",()=> {

      cb(matches);
   });

};

exports.getDeliveriesData = (cb) => {
   let deliveries = []
   let deliveriescsvPath = path.join(__dirname,"..","data","deliveries.csv");

    fs.createReadStream(deliveriescsvPath)
    .pipe(csv())
    .on("data",(data)=>deliveries.push(data))
    .on("end",()=>{

          cb(deliveries)
    });

};

exports.save = (returnedData, filename) =>{


    let modifiedFileName = "";
    if(filename.indexOf(".json")<= -1){

        modifiedFileName = filename + ".json"
    }
    else{

          modifiedFileName = filename
    }
    let saveLocation = path.join(__dirname,"..","public","output",`${modifiedFileName}`);

    fs.writeFile(saveLocation, JSON.stringify(returnedData),(err)=>{
        console.log(err);

    });
};


